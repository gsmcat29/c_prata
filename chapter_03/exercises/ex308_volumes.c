/*
In the U.S. system of volume measurements, a pint is 2 cups, a cup is 8 ounces, 
an ounce is 2 tablespoons, and a tablespoon is 3 teaspoons. Write a program that 
requests a volume in cups and that displays the equivalent volumes in pints, 
ounces, tablespoons, and teaspoons. Why does a floating-point type make more 
sense for this application than an integer type?
*/

#include <stdio.h>

int main(void)
{
    double cups = 0.0;
    double pints = 0.0;
    double ounces = 0.0;
    double tablespoons = 0.0;
    double teaspoons = 0.0;

    printf("Enter volume in  cups: ");
    scanf("%lf", &cups);

    pints = cups / 2;
    ounces = cups * 8;
    tablespoons = ounces * 2;
    teaspoons = tablespoons * 3;

    printf("Volume in pints:       %.2lf\n", pints);
    printf("Volume in ounces:      %.2lf\n", ounces);
    printf("Volume in tablespooms: %.2lf\n", tablespoons);
    printf("Volume in teaspoons:   %.2lf\n", teaspoons);
    
}


/**
 * Answer:
 * floating-point type make more sense for this applicatiog, in case any of the
 * volume measurements contains a decimal part to complete the equivalent
 * measurement
*/
