/*
Write a program that asks you to enter an ASCII code value, such as 66, and then 
prints the character having that ASCII code.
*/

#include <stdio.h>

int main(void)
{
    int value = 0;
    char ascii;

    printf("Enter ASCII code value: ");
    scanf("%d", &value);

    printf("The character is: ");
    ascii = value;
    printf("%c\n", ascii);

    return 0;
}