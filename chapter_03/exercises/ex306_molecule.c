/*
The mass of a single molecule of water is about 3.0×10-23 grams. A quart of 
water is about 950 grams. Write a program that requests an amount of water, in 
quarts, and displays the number of water molecules in that amount.
*/

#include <stdio.h>

int main(void)
{
    double water_molecule = 3.0e-23;
    double quart_water = 0.0;
    double total_grams = 0.0;
    double total_molecules = 0.0;

    printf("Enter amount of water, in quarts: ");
    scanf("%lf", &quart_water);

    total_grams = quart_water * 950;
    total_molecules = total_grams / water_molecule;

    printf("Total number of water molecules: %e\n", total_molecules);

    return 0;
}