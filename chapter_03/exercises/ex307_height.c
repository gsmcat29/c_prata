/*
There are 2.54 centimeters to the inch. Write a program that asks you to enter 
your height in inches and then displays your height in centimeters. Or, if you 
prefer, ask for the height in centimeters and convert that to inches.
*/

#include <stdio.h>

int main(void)
{
    double inches = 0.0;
    double centimeters = 0.0;

    printf("Enter height in centimeters: ");
    scanf("%lf", &centimeters);

    inches = centimeters / 2.54;

    printf("Height in inches: %.2lf\n", inches);

    return 0;
}