/*
Find out what your system does with integer overflow, floating-point overflow, 
and floating-point underflow by using the experimental approach; that is, write 
programs having these problems. (You can check the discussion in Chapter 4 of 
limits.h and float.h to get guidance on the largest and smallest values.)
*/

#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(void)
{
    // using limits.h
    printf("Limit of char: %d\n", CHAR_MAX);
    printf("Overflow of char: %d\n", CHAR_MAX + 1);

    printf("Limit of int: %d\n", INT_MAX);
    printf("Overflow of int: %d\n", INT_MAX + 1);


    return 0;
}