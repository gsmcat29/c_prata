/*
There are approximately 3.156 × 107 seconds in a year. Write a program that 
requests your age in years and then displays the equivalent number of seconds.
*/

#include <stdio.h>

int main(void)
{
    double second_year = 3.156e7;
    double number_secs = 0.0;
    int age = 0;

    printf("Enter your age in years: ");
    scanf("%d", &age);

    number_secs = age * second_year;
    printf("The number of seconds for your age is: %.2f\n", number_secs);

    return 0;
}