/* Write a function that returns the difference between the largest and smallest 
elements of an array-of-double. Test the function in a simple program.*/

#include <stdio.h>
#define SZ 5

double diff_elem(double *source);

int main(void)
{
    double source[SZ] = {5.5, 1.1, 12.2, 40.4, 3.3};


    double difference = 0.0;
    difference = diff_elem(source);
    printf("diff: %.2lf\n", difference);

    printf("Done\n");

    return 0;
}


double diff_elem(double *source)
{
    double num_a = 0.0;     // max value
    double num_b = 0.0;     // min value
    double diff = 0.0;

    num_a = source[0];
    num_b = source[0];

    for (int i = 1; i < SZ; ++i)
    {
        if (source[i] >= num_a)
        {
            num_a = source[i];
        }
        else if (source[i] <= num_b)
        {
            num_b = source[i];
        }
    }

    printf("Max value is: %.2lf\n", num_a);
    printf("Min value is: %.2lf\n", num_b);

    diff = num_a - num_b;

    return diff;
}

