/*
Write a program that initializes an array-of-double and then copies the contents 
of the array into three other arrays. (All four arrays should be declared in the 
main program.) To make the first copy, use a function with array notation. 
To make the second copy, use a function with pointer notation and pointer 
incrementing. Have the first two functions take as arguments the name of the 
target array, the name of the source array, and the number of elements to be 
copied. Have the third function take as arguments the name of the target, the 
name of the source, and a pointer to the element following the last element of 
the source. That is, the function calls would look like this, given the following
declarations:

double source[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
double target1[5];
double target2[5];
double target3[5];
copy_arr(target1, source, 5);
copy_ptr(target2, source, 5);
copy_ptrs(target3, source, source + 5);

*/

#include <stdio.h>

void copy_arr(double *dest, double *source, int sz);
void copy_ptr(double *dest, double *source, int sz);
void copy_ptrs(double *dest, double *source, double *position);

int main(void)
{
    double source[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
    double target1[5];
    double target2[5];
    double target3[5];    

    // array notation
    printf("array notation:\n");
    copy_arr(target1, source, 5);

    // display array notation =================================================
    for (int i = 0; i < 5; ++i)
    {
        printf("%.1lf ", target1[i]);
    }
    printf("\n");

    // pointer notation =======================================================
    printf("pointer notation:\n");
    copy_ptr(target2, source, 5);
    // display pointer notation 
    for (int i = 0; i < 5; ++i)
    {
        printf("%.1lf ", target2[i]);
    }
    printf("\n");    

    // pointer to element
    printf("pointer to element notation:\n");
    copy_ptrs(target3, source, source + 5);
    // display pointer element notation 
    for (int i = 0; i < 5; ++i)
    {
        printf("%.1lf ", target2[i]);
    }
    printf("\n"); 
    printf("Done\n");

    return 0;
}

void copy_arr(double *dest, double *source, int sz)
{
    for (int i = 0; i < sz; ++i)
    {
        dest[i] = source[i];
    }
}

void copy_ptr(double *dest, double *source, int sz)
{
    double *ptr_source;
    //ptr_source = &source[0];
    ptr_source = source;

    for (int i = 0; i < sz; i++)
    {
        dest[i] = *ptr_source;
        ptr_source++;
    }
}


void copy_ptrs(double *dest, double *source, double *position)
{
    int limit = 0;
    limit = sizeof(source) / sizeof(source[0]);
    //printf("limit = %d\n", limit);

    double *ptr_source;
    //ptr_source = &source[0];
    ptr_source = source;

    for (int i = 0; i < limit; i++)
    {
        dest[i] = *ptr_source;
        ptr_source++;
    }    
}