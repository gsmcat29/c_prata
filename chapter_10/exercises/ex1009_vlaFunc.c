/*
Write a program that initializes a two-dimensional 3×5 array-of-double and uses 
a VLA-based function to copy it to a second two-dimensional array. Also provide 
a VLA-based function to display the contents of the two arrays. 
The two functions should be capable, in general, of processing arbitrary N×M 
arrays. (If you don’t have access to a VLA-capable compiler, use the traditional 
C approach of functions that can process an N×5 array).
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROW 3
#define COL 5

void vla_display(int n_row, int n_col, double dest[ROW][COL], double source[ROW][COL]);
void vla_copy(int n_row, int n_col, double dest[ROW][COL], double source[ROW][COL]);

int main()
{
    double array_1[ROW][COL] = {0};
    double array_2[ROW][COL] = {0};
    int value = 0;

    for (int i = 0; i < ROW; ++i) {
        for (int j= 0; j < COL; ++j) {
            array_1[i][j] = value;
            value++;
        }
    }

    // call function
    int rs = 3;
    int cs = 5;
    vla_copy(rs, cs, array_2, array_1);
    vla_display(rs, cs, array_2, array_1);

    return 0;
}

void vla_copy(int n_row, int n_col, double dest[ROW][COL], double source[ROW][COL])
{
    for (int i = 0; i < ROW; ++i) {
        for (int j = 0; j < COL; ++j) {
            dest[i][j] = source[i][j];
        }
    }    
}

void vla_display(int n_row, int n_col, double dest[ROW][COL], double source[ROW][COL])
{
    printf("source array:\n");
    for (int i = 0; i < ROW; ++i) {
        for (int j= 0; j < COL; ++j) {
            printf("%5.2lf ", source[i][j]);
        }
        printf("\n");
    }

    printf("destination array:\n");
    for (int i = 0; i < ROW; ++i) {
        for (int j= 0; j < COL; ++j) {
            printf("%5.2lf ", dest[i][j]);
        }
        printf("\n");
    }    
}