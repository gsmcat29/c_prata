/*
Write a function that returns the largest value stored in an array-of-int. 
Test the function in a simple program.
*/

#include <stdio.h>

double maximum_value(double *source);
double maximum_ptr(double *source);

int main(void)
{
    double source[5] = {1.1, 2.2, 5.5, 40.4, 3.3};

    double max_source = maximum_value(source);    
    double max_source_ptr = maximum_ptr(source);

    printf("max value is %.1lf\n", max_source);
    printf("max value ptr is %.1lf\n", max_source_ptr);
    
    printf("Done\n");

    return 0;
}

double maximum_value(double *source)
{
    double max_value = source[0];

    for (int i = 0; i < 5; i++)
    {   
        if (source[i] > max_value)
            max_value = source[i];

    } 

    return max_value;
}


double maximum_ptr(double *source)
{
    double max_value;
    double *ptr_val = &source[0];

    for (int i = 0; i < 5; i++)
    {   
        if (*(source+i) > *ptr_val)
        {
            *ptr_val = *(source+i);
        }

    } 

    max_value = *ptr_val;

    return max_value;
}