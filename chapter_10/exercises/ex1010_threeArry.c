/*
Write a function that sets each element in an array to the sum of the 
corresponding elements in two other arrays. That is, if array 1 has the values 
2, 4, 5, and 8 and array 2 has the values 1, 0, 4, and 6, the function assigns 
array 3 the values 3, 4, 9, and 14. The function should take three array names 
and an array size as arguments. Test the function in a simple program.
*/

#include <stdio.h>
#include <stdlib.h>

void sum_array(int *arr1, int *arr2, int *arr3, int sz);

int main(void)
{
    int array_1[] = {2,4,5,8};
    int array_2[] = {1,0,4,6};
    int array_3[] = {0};

    int size_val = sizeof(array_1) / sizeof(array_1[0]);

    sum_array(array_1, array_2, array_3, size_val);

    printf("Values in array_3:\n");

    for (int i = 0; i < size_val; ++i) {
        printf("%d ", array_3[i]);
    }

    printf("\n");
    return 0;
}


void sum_array(int *arr1, int *arr2, int *arr3, int sz)
{
    for (int i = 0; i < sz; ++i) {
        arr3[i] = arr1[i] + arr2[i];
    }
}
