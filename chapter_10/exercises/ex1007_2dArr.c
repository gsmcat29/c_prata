/*
Write a program that initializes a two-dimensional array-of-double and uses one 
of the copy functions from exercise 2 to copy it to a second two-dimensional 
array. (Because a two-dimensional array is an array of arrays, a one-dimensional 
copy function can be used with each subarray.)
*/

#include <stdio.h>
#define ROWS 3
#define COLS 4

void copy_arr(double (*dest)[COLS], double (*source)[COLS]);

int main(void)
{
    double junk[ROWS][COLS] =
    {
        {2,4,6,8},
        {3,5,7,9},
        {12,10,8,6}
    };

    double copy_junk[ROWS][COLS] = {{0}};  // the whole array contains zeros

    printf("junk array:\n");
    for (int i = 0; i < ROWS; ++i)
    {
        for (int j = 0; j < COLS; ++j)
        {
            printf("%6.2lf ", junk[i][j]);
        }
        printf("\n");
    }

    printf("copy of junk array:\n");
    copy_arr(copy_junk, junk);


    printf("Done\n");

    return 0;
}


void copy_arr(double (*dest)[COLS], double (*source)[COLS])
{
    for (int i = 0; i < ROWS; ++i)
    {
        for (int j = 0; j < COLS; ++j)
        {
            dest[i][j] = source[i][j];
            printf("%6.2lf ", dest[i][j]);
        }
        printf("\n");
    }

    printf("test\n");
}