/*
Write a function that reverses the contents of an array of double and test it in 
a simple program.
*/

#include <stdio.h>
#define SZ 5

void reverse_array(double *source);

int main(void)
{
    double source[SZ] = {5.5, 1.1, 12.2, 40.4, 3.3};

    printf("Original array: ");
    for (int i = 0; i < SZ; ++i)
    {
        printf("%.2lf ", source[i]);
    }
    printf("\n");

    printf("Reverse string: ");
    reverse_array(source);

    return 0;
}


void reverse_array(double *source)
{
    for (int j = SZ-1; j >= 0; --j)
    {
        printf("%.2lf ", source[j]);
    }

    printf("\n");
}