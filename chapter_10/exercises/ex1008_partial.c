/*
Use a copy function from Programming Exercise 2 to copy the third through fifth
elements of a seven-element array into a three-element array. The function 
itself need not be altered; just choose the right actual arguments. (The actual 
arguments need not be an array name and array size. They only have to be the 
address of an array element and a number of elements to be processed.)
*/

#include <stdio.h>

void copy_arr(double *dest, double *source, int sz);
void copy_ptr(double *dest, double *source, int sz);
void copy_ptrs(double *dest, double *source, double *position);

int main(void)
{
    double source[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
    double target1[5];
    double target2[3]; 

    // array notation
    printf("array notation:\n");
    copy_arr(target1, source, 5);

    // display array notation =================================================
    for (int i = 0; i < 5; ++i)
    {
        printf("%.1lf ", target1[i]);
    }
    printf("\n");

    // pointer notation =======================================================
    printf("pointer notation:\n");
    copy_ptr(target2, source, 3);
    // display pointer notation 
    for (int i = 0; i < 3; ++i)
    {
        printf("%.1lf ", target2[i]);
    }
    printf("\n");    

    return 0;
}

void copy_arr(double *dest, double *source, int sz)
{
    for (int i = 0; i < sz; ++i)
    {
        dest[i] = source[i];
    }
}

void copy_ptr(double *dest, double *source, int sz)
{
    double *ptr_source;
    ptr_source = &source[2];
    //ptr_source = source;

    for (int i = 0; i < sz; i++)
    {
        dest[i] = *ptr_source;
        ptr_source++;
    }
}


