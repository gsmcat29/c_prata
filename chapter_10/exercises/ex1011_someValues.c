/*
Write a program that declares a 3×5 array of int and initializes it to some 
values of your choice. Have the program print the values, double all the values,
and then display the new values. Write a function to do the displaying and a 
second function to do the doubling. Have the functions take the array name and 
the number of rows as arguments.
*/

#include <stdio.h>
#include <stdlib.h>

#define ROWS 3
#define COLS 5

void display(int arry[ROWS][COLS]);
void double_val(int arry[ROWS][COLS]);

int main()
{
    
    int junk[ROWS][COLS] =
    {
        {2,4,6,8,7},
        {3,5,7,9,5},
        {12,10,8,6,3}
    };

    double_val(junk);
    display(junk);


    printf("Done\n");
    return 0;
}

void display(int arry[ROWS][COLS])
{
    // display contents of array
    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLS; ++j) {
            printf("%d ", arry[i][j]);
        }
        printf("\n");
    }
}

void double_val(int arry[ROWS][COLS])
{
    // double the contents
    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLS; ++j) {
            arry[i][j] = 2 * arry[i][j];
        }
    }
}
