/*
Do Programming Exercise 13, but use variable-length array function parameters.
*/
#include <stdio.h>
#include <stdlib.h>

#define ROWS 3
#define COLS 5

double set_avg(double a, double b, double c, double d, double e);
void display_results(double arry[ROWS][COLS], double *op_b, double op_c);

int main()
{
    double user_array[3][5];
    double avg_per_set[3] = {0};
    double total_avg = 0.0;
    int cnt = 0;
    double max_value = 0.0;

    printf("Enter three sets of five doubles:\n");

    while (cnt < 3) {
        scanf("%lf %lf %lf %lf %lf", 
        &user_array[cnt][0], &user_array[cnt][1], &user_array[cnt][2], 
        &user_array[cnt][3], &user_array[cnt][4]);

        avg_per_set[cnt] = set_avg(user_array[cnt][0], user_array[cnt][1], 
        user_array[cnt][2], user_array[cnt][3], user_array[cnt][4]);

        total_avg += user_array[cnt][0]+user_array[cnt][1]+user_array[cnt][2]+
        user_array[cnt][3]+user_array[cnt][4];

        ++cnt;
    }

    display_results(user_array, avg_per_set, total_avg);

    printf("\nDone\n");
    return 0;
}

double set_avg(double a, double b, double c, double d, double e)
{
    double avg_set = 0.0;

    avg_set = (a+b+c+d+e) / 5.0;

    return avg_set;
}

void display_results(double arry[ROWS][COLS], double *op_b, double op_c)
{
    double max_value = 0.0; 

    printf("display array ...\n\n");
    max_value = arry[0][0];
    // display array
    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLS; ++j) {
            if (arry[i][j] >= max_value)
                max_value = arry[i][j];
            printf("%10.2lf",arry[i][j]);
        }
        printf("\n");
    }

    printf("\n\n");

    printf("avg set 1: %5.2lf\n", op_b[0]);
    printf("avg set 2: %5.2lf\n", op_b[1]);
    printf("avg set 3: %5.2lf\n", op_b[2]);
    printf("total average: %5.2lf\n", op_c / (double) (COLS * ROWS) );
    printf("max value: %5.2lf\n", max_value);    
}