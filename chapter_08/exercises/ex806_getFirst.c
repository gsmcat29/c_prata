/*
Modify the get_first() function of Listing 8.8 so that it returns the first non-
whitespace character encountered. Test it in a simple program.
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

char get_first(void);

int main(void)
{
    char chr;

    printf("Enter a text that contains a non-whitespace character: ");
    chr = get_first();

    printf("The character is: %c\n", chr);

    return 0;
}

char get_first(void)
{
    int ch;

    ch = getchar();

    while (getchar() != '\n')
    {
        if (isspace(ch) || isblank(ch))
        {
            printf("you typed a non-whitespace character\nError\n");
            exit(1);
        }

        continue;
    }

    return ch;
}