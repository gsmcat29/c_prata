/*
Modify Programming Exercise 8 from Chapter 7 so that the menu choices are 
labeled by characters instead of by numbers; use q instead of 5 as the cue to 
terminate input.
*/

#include <stdio.h>
#include <stdlib.h>

#define PAY_RAT_1  8.75
#define PAY_RAT_2  9.33
#define PAY_RAT_3 10.00
#define PAY_RAT_4 11.20

#define TAX_RATE_1 0.15
#define TAX_RATE_2 0.20
#define TAX_RATE_3 0.25

int main(void)
{
    char selection = 0;

    double pay_rate = 0.0;
    double over_time = 0.0;
    double hours = 0.0;
    double hours_ot = 0.0;
    double total_pay = 0.0;
    double tax_pay = 0.0;
    double gross_pay = 0.0;

    while (selection != 'q')
    {
        printf("Enter the number correspodig to the desired pay rate or action:\n");
        printf("a) $8.75/hr\t\t\tb)$9.33/hr\n");
        printf("c)10.00/hr \t\t\td)$11.20/hr\n");
        printf("q)quit\n");

        scanf(" %c", &selection);

        switch (selection)
        {
        case 'a':
            pay_rate = PAY_RAT_1;
            break;
        case 'b':
            pay_rate = PAY_RAT_2;
            break;
        case 'c':
            pay_rate = PAY_RAT_3;
            break;
        case 'd':
            pay_rate = PAY_RAT_4;
            break;
        case 'q':
            exit(0);
        default:
            printf("Incorrect input. Try again\n");
            break;
        }

        printf("Enter the number of hours you worked: ");
        scanf("%lf", &hours);

        if (hours > 40)
        {
            hours_ot = hours - 40;
            over_time = hours_ot * (1.5 * pay_rate);
        }
        else 
        {
            hours_ot = 0;
            over_time = 0;
        }

        total_pay = (hours * pay_rate) + over_time;
        gross_pay = total_pay;

        if (total_pay >= 300) 
        {
            tax_pay = 300 * TAX_RATE_1;
            total_pay = total_pay - tax_pay;
        }
        
        if (total_pay >= 450)
        {
            tax_pay = (300 * TAX_RATE_1) + (150 * TAX_RATE_2);
            total_pay = total_pay - tax_pay;
        }

        if (hours_ot > 15)
        {
            tax_pay = (300 * TAX_RATE_1) + (150 * TAX_RATE_2);
            total_pay = total_pay - tax_pay;
            tax_pay = TAX_RATE_3 * total_pay;
            total_pay = total_pay - tax_pay;
        }

        printf("\n\nDisplaying results:\n");
        printf("Gross pay: %.2lf\n", gross_pay);
        printf("Net pay:   %.2lf\n", total_pay);
        printf("Taxes pay: %.2lf\n", tax_pay);


    }

    return 0;
}