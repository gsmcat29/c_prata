/*
Write a program that reads input as a stream of characters until encountering 
EOF. Have it report the average number of letters per word. Don’t count 
whitespace as being letters in a word. Actually, punctuation shouldn’t be 
counted either, but don’t worry about that now. (If you do want to worry about 
it, consider using the ispunct() function from the ctype.h family.)
*/


#include <stdio.h>
#include <ctype.h>

int main(void)
{
    char ch;
    int avg_per_word = 0;
    int cnt = 0;
    int sum = 0;
    int limiter = 0;

    while((ch = getchar()) != EOF)
    {
        if (isspace(ch) || ispunct(ch))
        {
            //cnt = 0;
            ++limiter;
        }
        else 
        {
            ++cnt;
            sum = cnt;
        }
    }

    avg_per_word = sum / limiter;
    printf("sum: %d, limit: %d\n", sum, limiter);
    printf("average of letters in a word: %d\n", avg_per_word);

    return 0;
}