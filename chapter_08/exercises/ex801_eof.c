/*
Devise a program that counts the number of characters in its input up to the end 
of file.
*/

#include <stdio.h>

int main(void)
{
    int counter = 0;
    char ch;

    printf("Enter character until EOF:\n");
    while((ch = getchar()) != EOF) // either EOF or &
    {
        ++counter;
        putchar(ch);
    }

    printf("\n# of characters: %d\n", counter);
    printf("Done\n");

    return 0;
}