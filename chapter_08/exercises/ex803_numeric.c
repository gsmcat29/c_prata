/*
Write a program that reads input as a stream of characters until encountering 
EOF. Have it report the number of uppercase letters, the number of lowercase 
letters, and the number of other characters in the input. You may assume that 
the numeric values for the lowercase letters are sequential and assume the same 
for uppercase. Or, more portably, you can use appropriate classification 
functions from the ctype.h library.
*/

#include <stdio.h>
#include <ctype.h>

int main(void)
{
    char ch;
    int n_lower = 0;
    int n_upper = 0;
    int n_other = 0;

    while ((ch = getchar()) != EOF)
    {
        if (islower(ch))
        {
            ++n_lower;
        }
        else if (isupper(ch))
        {
            ++n_upper;
        }
        else
            ++n_other;
    }

    printf("\n\n");
    printf("# of lowercase: %d\n", n_lower);
    printf("# of uppercase: %d\n", n_upper);
    printf("# of othercase: %d\n", n_other);

    return 0;
}