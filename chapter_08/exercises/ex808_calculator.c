/*
Write a program that shows you a menu offering you the choice of addition, 
subtraction, multiplication, or division. After getting your choice, the program 
asks for two numbers, then performs the requested operation. The program should 
accept only the offered menu choices. It should use type float for the numbers 
and allow the user to try again if he or she fails to enter a number. 
In the case of division, the program should prompt the user to enter a new value 
if 0 is entered as the value for the second number. A typical program run should 
look like this:

Enter the operation of your choice:
a. add
s. subtract
m. multiply
d. divide
q. quit
a
Enter first number: 22.4
Enter second number: one
one is not an number.
Please enter a number, such as 2.5, -1.78E8, or 3: 1
22.4 + 1 = 23.4
Enter the operation of your choice:
a. add
s. subtract
m. multiply
d. divide
q. quit
d
Enter first number: 18.4
Enter second number: 0
Enter a number other than 0: 0.2
18.4 / 0.2 = 92
Enter the operation of your choice:
a. add
s. subtract
m. multiply
d. divide
q. quit
q
Bye.
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(void)
{
    double number1 = 0.0;
    double number2 = 0.0;

    double sum = 0.0;
    double sub = 0.0;
    double mul = 0.0;
    double div = 0.0;

    char selection = 0;
    char op;

    while (selection != 'q')
    {
        printf("Enter the operation of your choice:\n");
        printf("a) add\t\t\ts) substract\n");
        printf("m) multiply\t\td) divide\n");
        printf("q)quit\n");        
        scanf(" %c", &selection);

        switch (selection)
        {
        case 'a':
            printf("add\n");
            op = '+';
            break;
        case 's':
            printf("subtract\n");
            op = '-';
            break;
        case 'm':
            printf("multiply\n");
            op = '*';
            break;
        case 'd':
            printf("divide\n");
            op = '/';
            break;
        case 'q':
            printf("Bye\n");
            exit(0);
        default:
            //printf("Try again, option not available\n");
            break;
        }

        printf("Enter first number: ");
        if(scanf("%lf",&number1) != 1)
        {
            printf("Please enter a number, such as 2.5, -1.78E8, or 3: ");
            //scanf(" %lf",&number1);
            continue;
        }
        printf("Enter second number: ");
        if(scanf("%lf",&number2) != 1)
        {
            printf("Please enter a number, such as 2.5, -1.78E8, or 3: ");
            //scanf(" %lf",&number2);
            continue;
        }

        //printf("out of loop: %lf\n", number1);
        //printf("out of loop: %lf\n", number2);
        if (op == '+')
        {
            sum = number1 + number2;
            printf("%.2lf + %.2lf = %.2lf\n", number1, number2, sum);
        }
        else if (op == '-')
        {
            sub = number1 - number2;
            printf("%.2lf - %.2lf = %.2lf\n", number1, number2, sub);
        }
        else if (op == '*')
        {
            mul = number1 * number2;
            printf("%.2lf * %.2lf = %.2lf\n", number1, number2, mul);
        }
        else if (op == '/')
        {
            if (number2 == 0)
            {
                printf("Enter a number other than 0: ");
                scanf("%lf", &number2);
            }

            div = number1 / number2;
            
            printf("%.2lf / %.2lf = %.2lf\n", number1, number2, div);
        }        
    }

    return 0;
}
