/*
Modify the guessing program of Listing 8.4 so that it uses a more intelligent 
guessing strategy. For example, have the program initially guess 50, and have it 
ask the user whether the guess is high, low, or correct. If, say, the guess is 
low, have the next guess be halfway between 50 and 100, that is, 75. If that 
guess is high, let the next guess be halfway between 75 and 50, and so on. 
Using this binary search strategy, the program quickly zeros in on the correct 
answer, at least if the user does not cheat.
*/

#include <stdio.h>

int main(void)
{   
    int guess = 50;
    int max_value = 100;
    int mid_value = 50;
    int low_value = 1;
    char response;

    printf("Pick an integer from 1 to 100. I will try to guess ");
    printf("it.\nRespond with a y if my guess is right and with");
    printf("\nan n if it is wrong\n");
    printf("high = h, low = l, correct = y\n");
    printf("uh..is your number %d?\n", mid_value);

    while((response = getchar()) != 'y')     
    { 
        if (response == 'h')
        {
            low_value = guess;
            guess = (guess + low_value - 5) / 2;
            printf("Well, then, is it %d?\n", guess);
            
        }
        else if (response == 'l')
        {
            max_value = guess;
            guess = (guess + max_value + 5) / 2;
            printf("Well, then is it %d?\n", guess);
        }
        else
            printf("Sorry, I understand only h, l  or y\n");
        while(getchar() != '\n')
            continue;                           // skip rest of input line
    }
    printf("I knew I could do it!\n");


    return 0;
}

/*int main(void)
{
    int guess = 1;
    char response;

    printf("Pick an integer from 1 to 100. I will try to guess ");
    printf("it.\nRespond with a y if my guess is right and with");
    printf("\nan n if it is wrong\n");
    printf("uh..is your number %d?\n", guess);

    while ((response = getchar()) != 'y')        // get response, compare to y
    {
        if (response == 'n')
            printf("Well, then, is it %d?\n", ++guess);
        else
            printf("Sorry, I understand only y or n\n");
        while(getchar() != '\n')
            continue;                           // skip rest of input line
    }
    printf("I knew I could do it!\n");

    return 0;
}*/