/*
Write a program that creates an eight-element array of ints and sets the 
elements to the first eight powers of 2 and then prints the values. Use a for 
loop to set the values, and, for variety, use a do while loop to display the 
values.
*/

// to compile, gcc -Wall -std=c11 ex613_power.c -lm

#include <stdio.h>
#include <math.h>

int main(void)
{
    int elements[8] = {0};
    int j = 0;

    for (int i = 0; i < 8; ++i) {
        elements[i] = (int) pow(2.0, i);
    }


    do {
        printf("elements[%d] = %d\n", j, elements[j]);
        ++j;
    } while (j < 8);

    return 0;
}