/*
Write a program that creates an array with 26 elements and stores the 26 
lowercase letters in it. Also have it show the array contents.
*/

#include <stdio.h>

int main(void)
{
    char alfabet[26];
    int k = 0;

    for (char i = 'a'; i <= 'z'; ++i) {
        alfabet[k] = i;
        ++k;
    }

    // display array
    for (int j = 0; j <= 25; ++j) {
        printf("%c ", alfabet[j]);
    }

    printf("\n");

    return 0;
}