/*
Write a program that reads eight integers into an array and then prints them in 
reverse order.
*/

#include <stdio.h>

int main(void)
{
    int read[8] = {0};      // every elements contains 0

    printf("Please enter 8 values for the array: ");
    for (int i = 0; i < 8; ++i) {
        scanf("%d", &read[i]);
    }

    printf("The reverse order of the array is: ");
    for (int j = 7; j >= 0; --j) {
        printf("%d ", read[j]);
    }

    printf("\n");

    return 0;
}