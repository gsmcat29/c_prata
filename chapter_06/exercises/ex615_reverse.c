/*
Write a program that reads in a line of input and then prints the line in 
reverse order.
You can store the input in an array of char; assume that the line is no longer 
than 255 characters. Recall that you can use scanf() with the %c specifier to 
read a character at a time from input and that the newline character (\n) is 
generated when you press the Enter key.
*/

#include <stdio.h>
#include <string.h>

int main(void)
{
    char line[255];
    int i = 0;
    int sz = 0;

    printf("Enter a line of input: ");
    
    do {
        scanf("%c", &line[i]);
        ++i;
    } while (line[i] != '\n'); // type ctrl - d

    // reverse order
    printf("Reversing line of input:\n");
    //printf("%ld\n", strlen(line));
    sz = strlen(line);

    /*for(int i = 0; i < strlen(line); ++i) {
        printf("%c", line[i]);
    }*/

    for (int j = strlen(line)+1; j >= 0; --j) {
        printf("%c", line[j]);
    }

    printf("\n");

    return 0;    
}