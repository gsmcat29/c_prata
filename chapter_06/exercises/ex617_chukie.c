/*
Chuckie Lucky won a million dollars (after taxes), which he places in an account 
that earns 8% a year. On the last day of each year, Chuckie withdraws $100,000. 
Write a program that finds out how many years it takes for Chuckie to empty his 
account.
*/

#include <stdio.h>

#define INTEREST 0.08

int main(void)
{
    double prize = 1000000;
    double withdraw = 100000;
    int n_years = 0;

    while (prize > 0) {
        prize = prize + (prize * INTEREST);
        prize = prize - withdraw;
        //printf("$%.2lf\n", prize);
        ++n_years;
    }

    printf("# years: %d\n", n_years);

    return 0;
}