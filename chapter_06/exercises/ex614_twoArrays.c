/*
Write a program that creates two eight-element arrays of doubles and uses a loop 
to let the user enter values for the eight elements of the first array. 
Have the program set the elements of the second array to the cumulative totals 
of the elements of the first array.
For example, the fourth element of the second array should equal the sum of the 
first four elements of the first array, and the fifth element of the second 
array should equal the sum of the first five elements of the first array. 
(It’s possible to do this with nested loops, but by using the fact that the 
fifth element of the second array equals the fourth element of the second array 
plus the fifth element of the first array, you can avoid nesting and just use a 
single loop for this task.) 
Finally, use loops to display the contents of the two arrays, with the first 
array displayed on one line and with each element of the second array displayed 
below the corresponding element of the first array.
*/

#include <stdio.h>

int main(void)
{
    double first_arr[8] = {0};
    double secon_arr[8] = {0};
    double sum = 0;

    printf("Enter the 8 elements for first_array: ");
    for (int i = 0; i < 8; ++i) {
        scanf("%lf", &first_arr[i]);
    }

    for (int j = 0; j < 8; ++j) {
        sum = sum + first_arr[j];
        secon_arr[j] = sum;
    }

    // display array: test
    printf("first array:    ");
    for (int k = 0; k < 8; ++k) {
        printf("%4.1lf ", first_arr[k]);
    }

    printf("\nsecon array:    ");
    for (int l = 0; l < 8; ++l) {
        printf("%4.1lf ", secon_arr[l]);
    }

    printf("\n");

    return 0;
}