/*
Consider these two infinite series:
1.0 + 1.0/2.0 + 1.0/3.0 + 1.0/4.0 + ...
1.0 - 1.0/2.0 + 1.0/3.0 - 1.0/4.0 + ...
Write a program that evaluates running totals of these two series up to some 
limit of number of terms. Hint: –1 times itself an odd number of times is –1, 
and –1 times itself an even number of times is 1. Have the user enter the limit 
interactively; let a zero or negative value terminate input. Look at the running 
totals after 100 terms, 1000 terms, 10,000 terms. Does either series appear to 
be converging to some value?
*/

#include <stdio.h>

int main(void)
{
    int limit = 0;
    double pos_series = 1.0;
    double neg_series = 1.0;
    int parity = 2;

    printf("Enter limit of terms: ");
    scanf("%d", &limit);
    
    while (1) {
        for (double i = 2; i <= limit+1; i += 1) {
            pos_series = pos_series + (1.0/i);

            if (parity % 2 == 0) {
                neg_series = neg_series - (1.0/i);
            }
            else {
                neg_series = neg_series + (1.0/i);
            }

            ++parity;
        }

        printf("positive infinite series: %lf\n", pos_series);
        printf("negative infinite series: %lf\n", neg_series);

        printf("Enter limit of terms: ");
        scanf("%d", &limit);

        if (limit == 0) {
            break;
        }

    }

    printf("Done\n");

    return 0;

}

/* Answer:

The negative series appears to be converging towards value 0.693....

Values for 100
positive infinite series: 5.197279
negative infinite series: 0.698073

Values for 1000
positive infinite series: 11.683748
negative infinite series: 0.391720

values for 10000


*/