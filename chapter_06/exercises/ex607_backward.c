/*
Write a program that reads a single word into a character array and then prints 
the word backward. Hint: Use strlen() (Chapter 4) to compute the index of the 
last character in the array
*/

#include <stdio.h>
#include <string.h>

int main(void)
{
    char word[80];
    size_t len;

    printf("Enter a word: ");
    scanf("%s", word);

    len = strlen(word);

    printf("Your word is %s\n", word);
    printf("size: %zd\n", len);

    printf("Backwards:\n");
    for(int i = len; i >= 0; --i) {
        printf("%c ", word[i]);
    }

    printf("\n");
    return 0;
}