/*
Write a program that prints a table with each line giving an integer, its square, 
and its cube. Ask the user to input the lower and upper limits for the table. 
Use a for loop
*/

#include <stdio.h>

int main(void)
{
    int lower = 0;
    int upper = 0;

    printf("Enter lower limit: ");
    scanf("%d", &lower);

    printf("Enter upper limit: ");
    scanf("%d", &upper);

    printf("number square cube\n");

    for (int i = lower; i <= upper; ++lower, ++i) {
        printf("%5d %5d %5d\n", i, i * i, i * i * i);
    }

    return 0;
}