/*
Daphne invests $100 at 10% simple interest. (That is, every year, the investment 
earns an interest equal to 10% of the original investment.) Deirdre invests $100 
at 5% interest compounded annually. (That is, interest is 5% of the current 
balance, including previous addition of interest.) Write a program that finds 
how many years it takes for the value of Deirdre’s investment to exceed the 
value of Daphne’s investment. Also show the two values at that time.
*/

#include <stdio.h>

#define DAPHNE_INTEREST 0.10
#define DEIRDRE_INTEREST 0.05

int main(void)
{
    double daphne_invest = 100;
    double daphne_original = 100;
    double deirdre_invest = 100;
    int n_years = 0;

    double daphne_total = 100;
    double deirdre_total = 0;

    // test simple interest for 5 years
    /*for (int i = 0 ; i < 5; ++i) 
    {
        daphne_total = daphne_invest + (daphne_original * DAPHNE_INTEREST);
        daphne_invest = daphne_total;
        printf("daphne: %.2lf\n", daphne_invest);
    }*/

    // test compunded annually for 5 years
    printf("\n\n");
    /*for (int j = 0; j < 5; ++j) {
        deirdre_total = deirdre_invest + (deirdre_invest * DEIRDRE_INTEREST);
        deirdre_invest = deirdre_total;
        printf("deirdre: %.2lf\n", deirdre_invest);
    }*/

    while (!(deirdre_invest > daphne_invest)) {
        ++n_years;
        daphne_total = daphne_invest + (daphne_original * DAPHNE_INTEREST);
        daphne_invest = daphne_total;
        printf("daphne: %.2lf\t\t", daphne_invest);

        deirdre_total = deirdre_invest + (deirdre_invest * DEIRDRE_INTEREST);
        deirdre_invest = deirdre_total;
        printf("deirdre: %.2lf\n", deirdre_invest);
    }

    printf("# of years: %d", n_years);

    printf("\n");
    return 0;
}