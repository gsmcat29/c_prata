/* Modify exercise 8 so that it uses a function to return the value of the 
calculation.*/

#include <stdio.h>

double difference(double a, double b);

int main(void)
{
    double number0 = 0.0;
    double number1 = 0.0;
    double answer = 0.0;

    printf("Enter number0 and number1: ");
    scanf("%lf %lf", &number0, &number1);


    while (1) {
        answer = difference(number0, number1);
        printf("%.2lf\n", answer);

        printf("Enter number0 (type q to exit): ");
        if(scanf("%lf %lf", &number0, &number1) != 2) {
            printf("EXIT!\n");
            break;
        }
    }


    return 0;
}

double difference(double a, double b)
{
    double result = (a - b) / (a * b);

    return result;
}