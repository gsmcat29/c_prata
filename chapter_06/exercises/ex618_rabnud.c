/*
Professor Rabnud joined a social media group. Initially he had five friends. He noticed that his friend count grew in the following fashion. The first week one friend dropped out and the remaining number of friends doubled. The second week two friends dropped out and the remaining number of friends doubled. In general, in the Nth week, N friends dropped out and the remaining number doubled. Write a program that computes and displays the number of friends each week. The program should continue until the count exceeds Dunbar’s number. Dunbar’s number is a rough estimate of the maximum size of a cohesive social group in which each member knows every other member and how they relate to one another. Its approximate value is 150.
*/

#include <stdio.h>

#define RABNUD_NUMBER 150

int main(void)
{
    int n_friends = 5;
    int n_years = 0;
    int i = 1;

    while (RABNUD_NUMBER > n_friends) {
        n_friends = n_friends - i;
        n_friends = n_friends * 2;
        ++i;
        ++n_years;
        printf("# of friends:\t%d\n", n_friends);
    }

    printf("# of years: %d\n", n_years);

    return 0;
}