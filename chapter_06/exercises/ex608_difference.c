/*
Write a program that requests two floating-point numbers and prints the value of 
their difference divided by their product. Have the program loop through pairs 
of input values until the user enters nonnumeric input.
*/

#include <stdio.h>

int main(void)
{
    double number0 = 0.0;
    double number1 = 0.0;
    double answer = 0.0;

    printf("Enter number0 and number1: ");
    scanf("%lf %lf", &number0, &number1);


    while (1) {
        answer = (number0 - number1) / (number0 * number1);
        printf("%.2lf\n", answer);

        printf("Enter number0 (type q to exit): ");
        if(scanf("%lf %lf", &number0, &number1) != 2) {
            printf("EXIT!\n");
            break;
        }
    }


    return 0;
}