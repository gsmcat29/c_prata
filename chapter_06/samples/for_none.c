/* for_none.c*/
/* You can even leave one or more expressions blank (do not omit semicolons)*/
#include <stdio.h>

int main(void)
{
    int ans, n;

    ans = 2;

    for (n = 3; ans <= 25; )
        ans = ans * n;
    
    printf("n = %d; ans = %d\n", n, ans);

    return 0;
}