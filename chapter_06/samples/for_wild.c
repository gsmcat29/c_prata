// for_wild.c
/* You can use any  legal expression you want for the third expression. 
   Whatever you put in will be updated for each iteration */
#include <stdio.h>

int main(void)
{
    int x;
    int y = 55;

    for (x = 1; y < 75; y = (++x * 5) + 50)
        printf("%10d %10d\n", x, y);
    
    return 0;
}