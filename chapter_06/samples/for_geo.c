// for_geo.c
/* You can let a quantity increase geometrically instead of arithmetically;
that is, instead of adding a fixed amount each time, you can multiply by a
fixed amount */
#include <stdio.h>

int main(void)
{
    double debt;

    for (debt = 100.0; debt < 150.0; debt = debt * 1.1)
        printf("Your debt is now $%.2f\n", debt);

    return 0;
}