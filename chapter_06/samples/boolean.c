// boolean.c -- using _Bool variable
#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    long num;
    long sum = 0l;
    //_Bool input_is_good;
    bool input_is_good;

    printf("Please enter an integer to be summed (q to quit): ");
    input_is_good = (scanf("%ld", &num) == true);

    while (input_is_good) {
        sum = sum + num;
        printf("Please enter next integer (q to quit): ");
        input_is_good = (scanf("%ld", &num) == 1);
    }

    printf("Those integer sum to %ld\n", sum);

    return 0;
}