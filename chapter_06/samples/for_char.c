// for_char.c
// you can count by characters instead of by numbers

#include <stdio.h>

int main(void)
{
    char ch;

    for (ch = 'a'; ch <= 'z'; ch++)
        printf("The ASCII value for %c is %d\n", ch, ch);
    
    return 0;
}