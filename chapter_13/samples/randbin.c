// randbin.c -- random access, binary i/o
#include <stdio.h>
#include <stdlib.h>

#define ARSIZE 100

int main()
{
    double numbers[ARSIZE];
    double value;
    const char *file = "numbers.dat";
    long pos;
    int i;
    FILE *iofile;

    // create a set fof double values
    for (i = 0; i < ARSIZE; i++)
        numbers[i] = 100.0 * i + 1.0 / (i + 1);
    // attempt to open file
    if ((iofile = fopen(file, "wb")) == NULL) {
        fprintf(stderr, "Could not open file\n");
        exit(EXIT_FAILURE);
    }

    // write array in binary format to file
    fwrite(numbers, sizeof(double), ARSIZE, iofile);
    fclose(iofile);

    if ((iofile = fopen(file, "rb")) == NULL) {
        fprintf(stderr, "Could not open file for random access\n");
        exit(EXIT_FAILURE);
    }

    // read selected items from file
    printf("Enter an index in the range 0-%d", ARSIZE-1);
    while (scanf("%d", &i) == 1 && i >= 0 && i <ARSIZE) {
        pos = (long) i * sizeof(double);        // calculate offset
        fseek(iofile, pos, SEEK_SET);           // go thete
        fread(&value, sizeof(double), 1, iofile);
        printf("The value there is %f\n", value);
        printf("NExt index (out of range to quit):\n");
    }

    // finish up
    fclose(iofile);
    puts("Bye!\n");

    return 0;
}