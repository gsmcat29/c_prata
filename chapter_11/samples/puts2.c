/* puts2.c -- prints a string and counts characters */
#include <stdio.h>

int puts2(const char *string)
{
    int count = 0;
    while (*string) // equivalent to string[i] != '\0'
    {
        putchar(*string++);
        count++;
    }

    putchar('\n');

    return(count);
}