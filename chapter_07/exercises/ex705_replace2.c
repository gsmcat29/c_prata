/* Redo exercise 4 using a switch. */

#include <stdio.h>

int main(void)
{
    int n_replace = 0;
    char ch;

    while((ch= getchar()) != '#')
    {
        switch (ch)
        {
        case '!':
            ++n_replace;
            printf("!!");
            break;
        case '.':
            ++n_replace;
            printf("!");
        default:
            printf("%c", ch);
            break;
        }

    }

    printf("\n# of replacements: %d\n", n_replace);

    return 0;
}