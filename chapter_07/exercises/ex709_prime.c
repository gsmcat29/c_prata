/*
Write a program that accepts a positive integer as input and then displays all 
the prime numbers smaller than or equal to that number.
*/

#include <stdio.h>
#include <stdbool.h>

int main() {
   int number = 0;
   int init = 2;
   bool isPrime;

   printf("Enter a positive integer: ");
   scanf("%d", &number);


   while (init <= number) {
      isPrime = true;

      for (int i = 2; i <= init / 2; ++i) 
      {

         if (init % i == 0) 
         {
            isPrime = false;
            break;
         }
      }

      if (isPrime)
         printf("%d ", init);

      ++init;
   }

   printf("\n");

   return 0;
}

