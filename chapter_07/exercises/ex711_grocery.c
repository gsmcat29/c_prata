/*
The ABC Mail Order Grocery sells artichokes for $2.05 per pound, beets for $1.15 
per pound, and carrots for $1.09 per pound. It gives a 5% discount for orders of 
$100 or more prior to adding shipping costs. It charges $6.50 shipping and 
handling for any order of 5 pounds or under, $14.00 shipping and handling for 
orders over 5 pounds and under 20 pounds, and $14.00 plus $0.50 per pound for 
shipments of 20 pounds or more. Write a program that uses a switch statement in 
a loop such that a response of a lets the user enter the pounds of artichokes 
desired, b the pounds of beets, c the pounds of carrots, and q allows the user 
to exit the ordering process. The program should keep track of cumulative totals. 
That is, if the user enters 4 pounds of beets and later enters 5 pounds of beets, 
the program should use report 9 pounds of beets. The program then should compute 
the total charges, the discount, if any, the shipping charges, and the grand 
total. The program then should display all the purchase information: the cost 
per pound, the pounds ordered, and the cost for that order for each vegetable, 
the total cost of the order, the discount (if there is one), the shipping charge, 
and the grand total of all the charges.
*/

#include <stdio.h>
#include <stdlib.h>

#define ARTICHOKES      2.05
#define BEETS           1.15      
#define CARROTS         1.09

#define DISCOUNT        0.05
#define SHIP_UNDER_5    6.50
#define SHIP_UNDER_20   14.00
#define PER_POUND       0.50   

int main(void)
{
    double total_carrots = 0.0;
    double total_artichokes = 0.0;
    double total_beets = 0.0;

    double pounds = 0.0;
    double remain = 0.0;
    double final_pounds = 0.0;

    double total_grocery = 0.0;         // grand total
    double total_discount = 0.0;

    char choice;

    printf("(a)rtichokes -- $2.05 lb\n");
    printf("(b)eets -- $1.15 lb\n");
    printf("(c)arrots -- $1.09 lb\n");
    printf("(d)isplay receipt\n");
    printf("(q)uit\n");

    while (1)
    {
        printf("Enter your choice: ");
        scanf(" %c", &choice);

        switch (choice)
        {
        case 'a':
            printf("Enter artichoke amount in lbs: ");
            scanf("%lf", &pounds);

            if (pounds <= 5)
            {
                total_artichokes += (pounds * ARTICHOKES);

                if (total_artichokes >  100)
                {
                    total_discount = total_artichokes * DISCOUNT;
                }
                total_artichokes -= total_discount;                
                total_artichokes += SHIP_UNDER_5;
                
            }
            else if (pounds > 5 && pounds < 20)
            {
                total_artichokes += (pounds * ARTICHOKES);

                if (total_artichokes >  100)
                {
                    total_discount = total_artichokes * DISCOUNT;
                }
                total_artichokes -= total_discount;
                total_artichokes += SHIP_UNDER_20;
            }
            else if (pounds >= 20)
            {
                remain = (pounds - 20) * PER_POUND;
                total_artichokes += (pounds * ARTICHOKES);

                if (total_artichokes >  100)
                {
                    total_discount = total_artichokes * DISCOUNT;
                }
                total_artichokes -= total_discount;                
                total_artichokes += SHIP_UNDER_20 + remain;
            }

            final_pounds += pounds;
            pounds = 0;
            total_discount = 0;

            break;
        case 'b':
            printf("Enter beets amount in lbs: ");
            scanf("%lf", &pounds);

            if (pounds <= 5)
            {
                total_beets += (pounds * ARTICHOKES);

                if (total_beets >  100)
                {
                    total_discount = total_beets * DISCOUNT;
                }
                total_beets -= total_discount;                
                total_beets += SHIP_UNDER_5;
                
            }
            else if (pounds > 5 && pounds < 20)
            {
                total_beets += (pounds * ARTICHOKES);

                if (total_beets >  100)
                {
                    total_discount = total_beets * DISCOUNT;
                }
                total_beets -= total_discount;
                total_beets += SHIP_UNDER_20;
            }
            else if (pounds >= 20)
            {
                remain = (pounds - 20) * PER_POUND;
                total_beets += (pounds * ARTICHOKES);

                if (total_beets >  100)
                {
                    total_discount = total_beets * DISCOUNT;
                }
                total_beets -= total_discount;                
                total_beets += SHIP_UNDER_20 + remain;
            }

            final_pounds += pounds;
            pounds = 0;
            total_discount = 0;

            break;
        case 'c':
            printf("Enter carrot amount in lbs: ");
            scanf("%lf", &pounds);

            if (pounds <= 5)
            {
                total_carrots += (pounds * ARTICHOKES);

                if (total_carrots >  100)
                {
                    total_discount = total_carrots * DISCOUNT;
                }
                total_carrots -= total_discount;                
                total_carrots += SHIP_UNDER_5;
                
            }
            else if (pounds > 5 && pounds < 20)
            {
                total_carrots += (pounds * ARTICHOKES);

                if (total_carrots >  100)
                {
                    total_discount = total_carrots * DISCOUNT;
                }
                total_carrots -= total_discount;
                total_carrots += SHIP_UNDER_20;
            }
            else if (pounds >= 20)
            {
                remain = (pounds - 20) * PER_POUND;
                total_carrots += (pounds * ARTICHOKES);

                if (total_carrots >  100)
                {
                    total_discount = total_carrots * DISCOUNT;
                }
                total_carrots -= total_discount;                
                total_carrots += SHIP_UNDER_20 + remain;
            }

            final_pounds += pounds;
            pounds = 0;
            total_discount = 0;
            break;
        case 'd':
            printf("***RECEIPT\n");
            printf("Artichoke total: $%.2lf\n", total_artichokes);            
            printf("Beets total: $%.2lf\n", total_beets); 
            printf("Carrots total: $%.2lf\n", total_carrots); 
            total_grocery = total_artichokes + total_beets + total_carrots;
            printf("Total of pound : %.2flf\n", final_pounds);
            printf("Grand Total: $%.2lf\n", total_grocery);
            break;
        case 'q':
            exit(0);
            break;
        default:
            break;
        }

    }



    return 0;
}
