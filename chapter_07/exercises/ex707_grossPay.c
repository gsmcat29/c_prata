/*
Write a program that requests the hours worked in a week and then prints the 
gross pay, the taxes, and the net pay. Assume the following:

a. Basic pay rate = $10.00/hr
b. Overtime (in excess of 40 hours) = time and a half
c. Tax rate: #15% of the first $300
20% of the next $150
25% of the rest

Use #define constants, and don’t worry if the example does not conform to current
tax law.
*/

#include <stdio.h>
#define PAY_RATE 10.00
#define TAX_RATE_1 0.15
#define TAX_RATE_2 0.20
#define TAX_RATE_3 0.25

int main(void)
{
    double over_time = 0.0;
    double hours = 0.0;
    double hours_ot = 0.0;
    double total_pay = 0.0;
    double tax_pay = 0.0;
    double gross_pay = 0.0;

    printf("Enter the number of hours you worked: ");
    scanf("%lf", &hours);

    if (hours > 40)
    {
        hours_ot = hours - 40;
        over_time = hours_ot * (1.5 * PAY_RATE);
    }
    else 
    {
        hours_ot = 0;
        over_time = 0;
    }

    total_pay = (hours * PAY_RATE) + over_time;
    gross_pay = total_pay;

    if (total_pay >= 300) 
    {
        tax_pay = 300 * TAX_RATE_1;
        total_pay = total_pay - tax_pay;
    }
    
    if (total_pay >= 450)
    {
        tax_pay = (300 * TAX_RATE_1) + (150 * TAX_RATE_2);
        total_pay = total_pay - tax_pay;
    }

    if (hours_ot > 15)
    {
        tax_pay = (300 * TAX_RATE_1) + (150 * TAX_RATE_2);
        total_pay = total_pay - tax_pay;
        tax_pay = TAX_RATE_3 * total_pay;
        total_pay = total_pay - tax_pay;
    }

    printf("\n\nDisplaying results:\n");
    printf("Gross pay: %.2lf\n", gross_pay);
    printf("Net pay:   %.2lf\n", total_pay);
    printf("Taxes pay: %.2lf\n", tax_pay);

    return 0;
}
