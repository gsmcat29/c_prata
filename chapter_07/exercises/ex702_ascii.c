/*
Write a program that reads input until encountering #. Have the program print 
each input character and its ASCII decimal code. Print eight character-code 
pairs per line.
Suggestion: Use a character count and the modulus operator (%) to print a 
newline character for every eight cycles of the loop.
*/

#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int counter = 0;
    char ch;

    printf("Enter your input, type # to finish:\n");

    while((ch = getchar()) != '#')
    {
        if (counter % 8 == 0)
            printf("\n");
        
        printf("%3d - %c ", ch, ch);
    }


    printf("\n");
    return 0;
}