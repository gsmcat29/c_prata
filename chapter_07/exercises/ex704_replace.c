/*
Using if else statements, write a program that reads input up to #, replaces 
each period with an exclamation mark, replaces each exclamation mark initially 
present with two exclamation marks, and reports at the end the number of 
substitutions it has made.
*/

#include <stdio.h>

int main(void)
{
    int n_replace = 0;
    char ch;

    while((ch= getchar()) != '#')
    {
        if (ch == '!')
        {
            ++n_replace;
            printf("!!");
        }
        else if (ch == '.')
        {   
            ++n_replace;
            ch = '!';
            printf("%c", ch);
        }
        else 
        {
            printf("%c", ch);
        }

    }

    printf("\n# of replacements: %d\n", n_replace);

    return 0;
}