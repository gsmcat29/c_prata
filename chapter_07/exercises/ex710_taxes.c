/*
The 1988 United States Federal Tax Schedule was the simplest in recent times. 
It had four categories, and each category had two rates. Here is a summary 
(dollar amounts are taxable income):

Category            Tax
Single              15% of first $17,850 plus 28% of excess
Head of Household   15% of first $23,900 plus 28% of excess
Married, Joint      15% of first $29,750 plus 28% of excess
Married, Separate   15% of first $14,875 plus 28% of excess

For example, a single wage earner with a taxable income of $20,000 owes 
0.15 × $17,850 + 0.28 × ($20,000−$17,850). Write a program that lets the user 
specify the tax category and the taxable income and that then calculates the tax. 
Use a loop so that the user can enter several tax cases.
*/

#include <stdio.h>
#include <stdlib.h>

#define EXCESS 0.28
#define NORMAL 0.15

#define SINGLE  17850
#define HEAD_H  23900
#define MARR_J  29750
#define MARR_S  14875

int main(void)
{
    int choice = 0;
    double income = 0.0;
    double tax = 0.0;
    
    while (1)
    {
        printf("TAX CATEGORIES:\n\n");
        printf("1) single               -- 15%% of first $17850 + 28%% of exces\n");
        printf("2) Head of Household    -- 15%% of first $23900 + 28%% of exces\n");
        printf("3) Married, Joint       -- 15%% of first $29750 + 28%% of exces\n");
        printf("4) Married, Separate    -- 15%% of first $14875 + 28%% of exces\n");
        printf("5) QUIT\n");

        printf("\nSelect a category:\n");
        scanf("%d", &choice);

        if (choice == 5)
            exit(0);

        printf("Enter your tabable income:\n");
        scanf("%lf", &income);

        switch (choice)
        {
        case 1:
            printf("Single category:\n\n");
            tax = (NORMAL * SINGLE) + EXCESS * (income - SINGLE);
            printf("Total tax: $%.2lf\n", tax);
            break;
        case 2:
            printf("Head of Household category:\n\n");
            tax = (NORMAL * HEAD_H) + EXCESS * (income - HEAD_H);
            printf("Total tax: $%.2lf\n", tax);
            break;
        case 3:
            printf("Married, Joint category:\n\n");
            tax = (NORMAL * MARR_J) + EXCESS * (income - MARR_J);
            printf("Total tax: $%.2lf\n", tax);
            break;
        case 4:
            printf("Married, Separate category\n\n");
            tax = (NORMAL * MARR_S) + EXCESS * (income - MARR_S);
            printf("Total tax: $%.2lf\n", tax);
            break;                    
        default:
            printf("Incorrect option. Try again\n");
            break;
        }
    }



    printf("\n");
    return 0;
}