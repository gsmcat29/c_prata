/*
Write a program that reads input until encountering the # character and then 
reports the number of spaces read, the number of newline characters read, and 
the number of all other characters read.
*/

#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int newline = 0;
    int spaces = 0;
    int other = 0;
    char ch;

    printf("Enter your input, type # to finish:\n");

    while((ch = getchar()) != '#')
    {
        if (ch == '\n')
            ++newline;
        if (isspace(ch))
            ++spaces;
        if (isalnum(ch) || ispunct(ch))
            ++other;
    }

    printf("Displaying results:\n");
    printf("# of spaces:    %d\n", spaces);
    printf("# of newlines:  %d\n", newline);
    printf("# of all other: %d\n", other);

    return 0;
}