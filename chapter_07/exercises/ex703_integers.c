/*
Write a program that reads integers until 0 is entered. After input terminates, 
the program should report the total number of even integers (excluding the 0) 
entered, the average value of the even integers, the total number of odd 
integers entered, and the average value of the odd integers
*/

#include <stdio.h>

int main(void)
{
    int numbers;
    int n_even = 0;
    int n_odd = 0;

    double avg_even = 0;
    double avg_odd = 0;

    printf("Enter numbers until you type 0:\n");

    while (1) 
    {
        scanf("%d", &numbers);

        if (numbers == 0)
            break;

        if (numbers % 2 == 0)
        {
            ++n_even;
            avg_even += numbers;
        }
        else
        {
            ++n_odd;
            avg_odd += numbers;
        }
    }

    // compute values
    avg_even = avg_even / n_even;
    avg_odd = avg_odd / n_odd;

    // display result
    printf("total number of even integers:  %d\n", n_even);
    printf("Average value of even integers: %.2lf\n", avg_even);
    printf("total number of odd integers:   %d\n", n_odd);
    printf("Average value of odd integers:  %.2lf\n", avg_odd);    
    

    return 0;
}