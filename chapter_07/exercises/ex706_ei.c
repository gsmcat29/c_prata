/*
Write a program that reads input up to # and reports the number of times that 
the sequence ei occurs.
Note
The program will have to “remember” the preceding character as well as the 
current character.
Test it with input such as “Receive your eieio award.”
*/

#include <stdio.h>
#include <string.h>

int main(void)
{
    char sentence[120];
    char ch;
    int i = 0;
    size_t len = 0;
    int n_sequence = 0;

    printf("Enter a sentence:\n");
    while ((ch = getchar()) != '\n')
    {
        sentence[i] = ch;
        if (sentence[i] == 'i' && sentence[i-1] == 'e')
            ++n_sequence;
        ++i;
    }


    len = strlen(sentence);
    printf("len: %zd\n", len);
    printf("sentence: %s\n", sentence);

    printf("# of sequences: %d\n", n_sequence);
    return 0;
}