// nested if test
#include <stdio.h>
int main(void)
{
    int num = 0;
    int div = 0;

    printf("Enter a number: ");
    scanf("%d", &num);

    for (div = 2; (div * div) <= num; div++) {
        if (num % div == 0)
            printf("%d is divisible by %d and %d\n", num, div, num / div);
        else
            printf("%d is divisible by %d\n", num, div);
    }


    return 0;
}