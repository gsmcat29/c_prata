/*
Write a program that converts your age in years to days and displays both values. 
At this point, don’t worry about fractional years and leap years.
*/

#include <stdio.h>

int main(void)
{
    int age = 30;
    int to_days = 0;

    to_days = 365.0 * age;

    printf("My age is years: %d\n", age);
    printf("My age in days: %d\n", to_days);

    return 0;
}