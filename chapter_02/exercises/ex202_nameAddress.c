/* Write a program to print your name and address */
#include <stdio.h>

int main(void)
{
    printf("Name: John Doe\n");
    printf("Address: 8401 Gateway Blvd W., El Paso TX 79925\n");

    return 0;
}