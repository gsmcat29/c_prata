/*
Write a program that creates an integer variable called toes. Have the program 
set toes to 10. Also have the program calculate what twice toes is and what toes 
squared is. The program should print all three values, identifying them.
*/

#include <stdio.h>

int main(void)
{
    int toes = 10;
    printf("toes = %d\n", toes);
    printf("twice toes = %d\n", toes * 2);
    printf("toes squared = %d\n", toes * toes);

    return 0;
}