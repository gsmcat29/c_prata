// book.c -- one-book inventory
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *s_gets(char *st, int n);

#define MAXTITL 41  // maximum length of title + 1
#define MAXAUTL 31  // maximum length of aurhor's name + 1

struct book
{
    char title[MAXTITL];
    char author[MAXAUTL];
    float value;
};

int main(void)
{
    struct book library;   // declare library as a book variable

    printf("Enter the book title\n");
    s_gets(library.title, MAXTITL);
    printf("Enter author\n");
    s_gets(library.author, MAXAUTL);
    printf("Enter the value:\n");
    scanf("%f", &library.value);

    printf("%s by %s: $%.2f\n", library.title ,library.author, library.value);
    printf("%s: \"%s\" ($%.2f)\n", library.author, library.title, library.value);
    printf("Done\n");

    return 0;
}

char *s_gets(char *st, int n)
{
    char *ret_val;
    char *find;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        find = strchr(st, '\n');    // look for new line
        if (find)                   // if address not NULL, place null character
            *find = '\0';
        else
            while (getchar() != '\n')
                continue;           // dispose of rest of line
    }

    return ret_val;
}