/*
Write a program that asks the user to enter a height in centimeters and then 
displays the height in centimeters and in feet and inches. Fractional 
centimeters and inches should be allowed, and the program should allow the user 
to continue entering heights until a nonpositive value is entered. A sample run 
should look like this: 

Enter a height in centimeters: 182
182.0 cm = 5 feet, 11.7 inches
Enter a height in centimeters (<=0 to quit): 168.7
168.0 cm = 5 feet, 6.4
inches
Enter a height in centimeters (<=0 to quit): 0
bye

*/
#include <stdio.h>

#define CM_FEET      0.0328084
#define FEET_INCHES  12

int main(void)
{
    double centimeters = 0.0;
    double feet = 0.0;
    double inches = 0.0;

    printf("Enter a height in centimeters: ");
    scanf("%lf", &centimeters);

    while (centimeters > 0) {
        feet = centimeters * CM_FEET;
        inches = (feet - (int) feet) * FEET_INCHES;
        //printf("%lf\n", feet);
        //printf("%lf\n", inches);
        printf("%.1lf cm = %d feet, %.1lf inches\n", centimeters, (int) feet, inches);

        printf("Enter a height in centimeters (<= 0 to quit): ");
        scanf("%lf", &centimeters);
    }



    return 0;
}