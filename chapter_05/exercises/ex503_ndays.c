/*
Write a program that asks the user to enter the number of days and then converts 
that value to weeks and days. For example, it would convert 18 days to 2 weeks, 
4 days. 

Display results in the following format:

18 days are 2 weeks, 4 days.

Use a while loop to allow the user to repeatedly enter day values; terminate the 
loop when the user enters a nonpositive value, such as 0 or -20.
*/

#include <stdio.h>

int main(void)
{
    int n_days = 1;
    int weeks  = 0;
    int days   = 0;

    while (n_days > 0) {
        printf("Enter number of days: ");
        scanf("%d", &n_days);

        weeks = n_days / 7;
        days = n_days % 7;

        printf("%d days are %d weeks, %d days.\n", n_days, weeks, days);
    }

    return 0;
}