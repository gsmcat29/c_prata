/*
Write a program that converts time in minutes to time in hours and minutes. Use
#define or const to create a symbolic constant for 60. Use a while loop to allow 
the user to enter values repeatedly and terminate the loop if a value for the 
time of 0 or less is entered.
*/

#include <stdio.h>

#define TIME_C 60

int main(void)
{
    int minutes = 0;
    int hours = 0;
    int remain_minutes = 0;

    while (minutes >= 0) {
        printf("Enter number of minutes (-1 to exit): ");
        scanf("%d", &minutes);

        hours = minutes / TIME_C;
        remain_minutes = minutes % TIME_C;

        printf("%d hour(s) with %d minutes\n", hours, remain_minutes);
    }


    return 0;
}