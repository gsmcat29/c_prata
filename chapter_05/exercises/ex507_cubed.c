/*
Write a program that requests a type double number and prints the value of the 
number cubed. Use a function of your own design to cube the value and print it. 
The main() program should pass the entered value to this function.
*/

#include <stdio.h>

double cubed(double n);

int main(void)
{
    double number = 0.0;
    double cubed_value = 0.0;

    printf("Enter a number: ");
    scanf("%lf", &number);

    cubed_value = cubed(number);

    printf("The cubed value is: %.2lf\n", cubed_value);

    return 0;
}

double cubed(double n)
{
    return (n * n * n);
}