/*
Write a program that requests the user to enter a Fahrenheit temperature. 
The program should read the temperature as a type double number and pass it as 
an argument to a user-supplied function called Temperatures(). This function 
should calculate the Celsius equivalent and the Kelvin equivalent and display 
all three temperatures with a precision of two places to the right of the 
decimal. It should identify each value with the temperature scale it represents. 
Here is the formula for converting Fahrenheit to Celsius: 

Celsius = 5.0 / 9.0 * (Fahrenheit - 32.0)

The Kelvin scale, commonly used in science, is a scale in which 0 represents 
absolute zero, the lower limit to possible temperatures. Here is the formula 
for converting Celsius to Kelvin:

Kelvin = Celsius + 273.16

The Temperatures() function should use const to create symbolic representations 
of the three constants that appear in the conversions. The main() function 
should use a loop to allow the user to enter temperatures repeatedly, stopping 
when a q or other nonnumeric value is entered. Use the fact that scanf() 
returns the number of items read, so it will return 1 if it reads a number, 
but it won’t return 1 if the user enters q. The == operator tests for equality, 
so you can use it to compare the return value of scanf() with 1.
*/

#include <stdio.h>
#include <ctype.h>

void temperatures(double f);

int main(void)
{
    double fahrenheit = 0.0;

    printf("Enter value of temperature in *F: ");
    scanf("%lf", &fahrenheit);

    while (1) {
        temperatures(fahrenheit);
        printf("Enter value of Temperature in *F (q to quit): ");
        if (scanf("%lf", &fahrenheit) == 1) {
            ;
        }
        else {
            break;
        }
    }

    printf("Done\n");

    return 0;
}

void temperatures(double f)
{
    const double KELVIN_FACTOR = 273.16;
    const double CELSIUS_FACTOR = 5.0/9.0;

    double celsius = 0.0;
    double kelvin = 0.0;

    celsius = CELSIUS_FACTOR * (f - 32.0);
    kelvin = KELVIN_FACTOR + celsius;

    printf("fahrentheit: %8.2lf\n", f);
    printf("celsius    : %8.2lf\n", celsius);
    printf("kelvin     : %8.2lf\n", kelvin);

}
