/*
Now modify the program of Programming Exercise 5 so that it computes the sum of 
the squares of the integers. (If you prefer, how much money you receive if you 
get $1 the first day, $4 the second day, $9 the third day, and so on. This looks 
like a much better deal!) C doesn’t have a squaring function, but you can use 
the fact that the square of n is n * n.
*/

#include <stdio.h>

int main(void)                      // finds sum of first 20 integers
{
    int count, square;                 // declaration statement
    int limit = 0;

    printf("Enter the limit value: ");
    scanf("%d", &limit);

    count = 0;                      // assigment statement
    square = 0;                      // ditto

    while (count++ < limit) {          // while
        square = count * count;        //      statement
    }

    printf("sum = %d\n", square);      // function statement

    return 0;                       // return statement
}