/*
Generalize the to_binary() function of Listing 9.8 to a to_base_n() function 
that takes a second argument in the range 2–10. It then should print the number 
that is its first argument to the number base given by the second argument. 
For example, to_base_n(129,8) would display 201, the base-8 equivalent of 129. 
Test the function in a complete program.
*/
#include <stdio.h>

void to_binary(unsigned long n);
void to_base_n(unsigned long n, int base);

int main(void)
{
    unsigned long number;
    int n_base;

    printf("Enter number, base (q to quit):\n");

    while (scanf("%lu %d", &number, &n_base) == 2)
    {
        printf("Base %d equivalent: ", n_base);
        //to_binary(number);
        to_base_n(number, n_base);
        putchar('\n');
        printf("Enter an integer (q to quit):\n");
    }

    printf("done\n");

    return 0;
}

void to_binary(unsigned long n)     // recursive function
{
    int r;

    r = n % 2;
    if (n >= 2)
        to_binary(n/2);
    putchar(r == 0 ? '0' : '1');

    return;
}

void to_base_n(unsigned long n, int base)
{
    int rem = 0;
    int i = 0;
    int value[64];

    if (base == 10)
        printf("%zd", n);
    else if (base == 2)
        to_binary(n);
    else
    {
        while (n  != 0)
        {
            rem = n % base;
            value[i] = rem;
            ++i;
            n =  n / base;
        }

        // print array
        for (int j = i-1; j >= 0; --j)
        {
            printf("%d", value[j]);
        }

    }
}