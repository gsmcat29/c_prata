/*
Write a program that reads characters from the standard input to end-of-file. 
For each character, have the program report whether it is a letter. 
If it is a letter, also report its numerical location in the alphabet. 
For example, c and C would both be letter 3. Incorporate a function that takes a 
character as an argument and returns the numerical location if the character is 
a letter and that returns –1 otherwise.
*/

#include <stdio.h>
#include <ctype.h>

int numerical_location(char ch);

int main(void)
{
    char letter;
    int position = 0;

    printf("Enter a alphabet letter, type # t quit: ");
    while(scanf(" %c", &letter) == 1)
    {
        if (letter == '#')
            break;
        position = numerical_location(letter);
        printf("position: %d\n", position);
        printf("Enter a alphabet letter, type # t quit: ");
        
    }



    printf("Done\n");

    return 0;
}

int numerical_location(char ch)
{
    int number = 0;
    char upper_case;

    upper_case = toupper(ch);

    //printf("Uppercase: %c\n", upper_case);
    number = upper_case - '@';

    return number;
}