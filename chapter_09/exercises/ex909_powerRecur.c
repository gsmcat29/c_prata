/* Redo Programming Exercise 8, but this time use a recursive function. */
#include <stdio.h>
#include <stdbool.h>

double power(double n, int p);      // ANSI prototype

int main(void)
{
    double x, xpow;
    int exp;

    printf("Enter a number and the positive integer power");
    printf(" to which\nthe number will be raised. Enter q to quit\n");

    while (scanf("%lf%d", &x, &exp) == 2) {
        xpow = power(x, exp);         // function call
        printf("%.3g to the power %d is %.5g\n", x, exp, xpow);
        printf("Enter next pair of numbers or q to quit\n");
    }

    printf("Hope you enjoyded this power trip -- bye!\n");

    return 0;
}

double power(double n, int p)       // function definition
{
    double pow = 1;
    bool isNegative;
    int i;

    isNegative = false;

    if (n < 0 && (int) p % 2 == 1)
    {
        n = -1 * n;
        isNegative = true;
    }
    else if( n == 0 && p == 0)
    {
        printf("0 to the 0 is undefined\n");
    }
    else if (n == 0)
    {
        pow = 0;
        return pow;
    }
    else if (p == 0)
    {
        pow = 1;
        return pow;
    }


    if(p != 0)
    {
        pow =  (n * power(n, p-1));
    }
    else
        pow = 1;

    if (isNegative)
        pow = -pow;
    
    return pow;                     // return the value of pow
}