/*
Write and test a function that takes the addresses of three double variables as 
arguments and that moves the value of the smallest variable into the first 
variable, the middle value to the second variable, and the largest value into 
the third variable.
*/

#include <stdio.h>

void middle(double var_1, double var_2, double var_3);

int main(void)
{
    double number_a = 0.0;
    double number_b = 0.0;
    double number_c = 0.0;

    printf("Enter three numbers (q to quit): ");
    while(scanf("%lf %lf %lf", &number_a, &number_b, &number_c) == 3)
    {
        middle(number_a, number_b, number_c);
        printf("Enter three numbers (q to quit): ");

    }

    printf("Done\n");

    return 0;
}

void middle(double var_1, double var_2, double var_3)
{
    double first = 0.0;
    double second = 0.0;
    double third = 0.0;

    if (var_1 > var_2 && var_1 > var_3 )
    {
        third = var_1;
        if (var_2 < var_3)
        {
            first = var_2;
            second = var_3;
        }
        else
        {
            first = var_3;
            second = var_2;
        }
    }
    else if (var_2 > var_1 && var_2 > var_3 )
    {
        third = var_2;
        if (var_1 < var_3)
        {
            first = var_1;
            second = var_3;
        }
        else
        {
            first = var_3;
            second = var_1;
        }
    }
    else if (var_3 > var_1 && var_3 > var_2 )
    {
        third = var_3;
        if (var_1 < var_2)
        {
            first = var_1;
            second = var_2;
        }
        else
        {
            first = var_2;
            second = var_1;
        }
    }
    else
        printf("All numbers are the same\n");
    
    printf("first value:  %.2lf\n", first);
    printf("second value: %.2lf\n", second);
    printf("third value:  %.2lf\n", third);
    
}