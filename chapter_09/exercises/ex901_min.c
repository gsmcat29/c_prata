/*
Devise a function called min(x,y) that returns the smaller of two double values. 
Test the function with a simple driver.
*/

#include <stdio.h>

double min(double x, double y);

int main(void)
{
    double value1 = 0.0;
    double value2 = 0.0;
    double min_value = 0.0;

    printf("Please enter two values: (q to quit): ");
    while (scanf("%lf %lf", &value1, &value2) == 2)
    {
        min_value = min(value1, value2);
        printf("Min value is %.2lf\n", min_value);
        printf("Please enter two values: (q to quit): ");        
    }

    printf("Done\n");

    return 0;
}

double min(double x, double y)
{
    if (x < y)
        return x;
    else
        return y;
}

