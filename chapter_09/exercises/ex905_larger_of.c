/*
Write and test a function called larger_of() that replaces the contents of two 
double variables with the maximum of the two values. For example, larger_of(x,y) 
would reset both x and y to the larger of the two.
*/

#include <stdio.h>

void larger_of(double x, double y);

int main(void)
{
    double number_a = 0.0;
    double number_b = 0.0;

    printf("Enter two numbers (q to quit): ");
    while(scanf("%lf %lf", &number_a, &number_b) == 2)
    {
        larger_of(number_a, number_b);
        printf("Eter two numbers (q to quit): ");
 
    }
    
    printf("Done\n");

    return 0;
}

void larger_of(double x, double y)
{
    if (x > y)
    {
        y =  x;
        printf("New values are: %.2lf %.2lf\n", x, y);
    }
    else 
    {
        x = y;
        printf("New values are: %.2lf %.2lf\n", x, y);
    }
}