/*
Write and test a Fibonacci() function that uses a loop instead of recursion to 
calculate Fibonacci numbers.
*/

#include <stdio.h>
int fibonacci(int n, int m, int qty);

int main(void)
{
    int f0 = 0;
    int f1 = 1;
    int fn = 0;
    int limit = 0;

    printf("Enter # of digits for fibonacci: (q to quit): ");
    while(scanf("%d", &limit) == 1)
    {
        fn = fibonacci(f0, f1, limit);
        printf("Last fibonacci number: %d\n", fn);
        printf("Enter # of digits for fibonacci: (q to quit): ");
    }



    printf("Done!\n");
    return 0;
}

int fibonacci(int n, int m, int qty)
{
    int f_n;
    int cnt = 0;

    while (cnt < qty)
    {
        f_n = n + m; 
        n = m;
        m = f_n;
        ++cnt;
    }

    return f_n;

}