/*
Write a function that takes three arguments: a character and two integers. 
The character is to be printed. The first integer specifies the number of times 
that the character is to be printed on a line, and the second integer specifies 
the number of lines that are to be printed. Write a program that makes use of 
this function.
*/

#include <stdio.h>

void chline(char ch, int i, int j);

int main()
{
    char symbol;
    int row = 0;
    int col = 0;

    printf("Please enter # of rows, # of cols and symbol: (q to  quit): ");
    while (scanf("%d %d %c", &row, &col, &symbol) == 3)
    {
        chline(symbol, row, col);
        printf("Please enter # of rows, # of cols and symbol: (q to  quit): ");
    }

    printf("Done\n");

    return 0;
}


void chline(char ch, int i, int j)
{
    for (int a = 0; a < i; ++a) 
    {
        for (int b = 0; b < j; ++b)
        {
            printf("%c ", ch);
        }
        printf("\n");
    }

}   