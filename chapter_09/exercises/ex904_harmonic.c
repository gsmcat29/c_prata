/*
The harmonic mean of two numbers is obtained by taking the inverses of the two
numbers, averaging them, and taking the inverse of the result. Write a function 
that takes two double arguments and returns the harmonic mean of the two numbers.
*/

#include <stdio.h>

double harmonic(double number_a, double number_b);

int main(void)
{
    double num1 = 0.0;
    double num2 = 0.0;
    double harmonic_mean = 0.0;

    printf("Please enter two numbers (q to quit): ");

    while (scanf("%lf %lf", &num1, &num2) == 2)
    {
        harmonic_mean = harmonic(num1, num2);

        printf("Harmonic means = %.2lf\n", harmonic_mean);
        printf("Please enter two numbers (q to quit): ");
    }
    printf("Done\n");

    return 0;
}


double harmonic(double number_a, double number_b)
{
    double answer = 0;

    answer = 2.0 / (1/number_a + 1/number_b);

    return answer;
}