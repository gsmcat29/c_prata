/*
Devise a function chline(ch,i,j) that prints the requested character in columns i
through j. Test it in a simple driver.
*/

#include <stdio.h>

void chline(char ch, int i, int j);

int main()
{
    char symbol;
    int row = 0;
    int col = 0;

    printf("Please enter # of rows, # of cols and symbol: (q to  quit): ");
    while (scanf("%d %d %c", &row, &col, &symbol) == 3)
    {
        chline(symbol, row, col);
        printf("Please enter # of rows, # of cols and symbol: (q to  quit): ");
    }

    printf("Done\n");

    return 0;
}


void chline(char ch, int i, int j)
{
    for (int a = 0; a < i; ++a) 
    {
        for (int b = 0; b < j; ++b)
        {
            printf("%c ", ch);
        }
        printf("\n");
    }

}