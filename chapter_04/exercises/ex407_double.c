/*
Write a program that sets a type double variable to 1.0/3.0 and a type float 
variable to 1.0/3.0. Display each result three times—once showing four digits to 
the right of the decimal, once showing 12 digits to the right of the decimal, 
and once showing 16 digits to the right of the decimal. Also have the program 
include float.h and display the values of FLT_DIG and DBL_DIG. 
Are the displayed values of 1.0/3.0 consistent with these values?
*/

#include <stdio.h>
#include <float.h>

int main(void)
{
    double double_value = 1.0 / 3.0;
    float  float_value = 1.0 / 3.0;

    printf("double variable results:\n");
    printf("%.4lf\n", double_value);
    printf("%.12lf\n", double_value);
    printf("%.16lf\n\n", double_value);

    printf("float variable results:\n");
    printf("%.4f\n", float_value);
    printf("%.12f\n", float_value);
    printf("%.16f\n\n", float_value);

    printf("FLT_DIG = %d\n", FLT_DIG);
    printf("DBL_DIG = %d\n", DBL_DIG);

    return 0;   
}

/**
 * asnwer:
 * They are not consistant, only the double value have the same digits
*/