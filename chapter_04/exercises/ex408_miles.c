/*
Write a program that asks the user to enter the number of miles traveled and the 
number of gallons of gasoline consumed. It should then calculate and display the 
miles-per-gallon value, showing one place to the right of the decimal. 
Next, using the fact that one gallon is about 3.785 liters and one mile is about 
1.609 kilometers, it should convert the mile-per-gallon value to a 
liters-per-100-km value, the usual European way of expressing fuel consumption, 
and display the result, showing one place to the right of the decimal. 
Note that the U.S. scheme measures the distance traveled per amount of fuel 
(higher is better), whereas the European scheme measures the amount of fuel per 
distance (lower is better). 
Use symbolic constants (using const or #define) for the two conversion factors.
*/

#include <stdio.h>

#define GALLON_FACTOR 3.785
#define MILE_FACTOR   1.609

int main(void)
{
    double miles_traveled = 0.0;
    double number_gallons = 0.0;
    double miles_gallon = 0.0;      // miles / gallon
    double km_travel = 0.0;
    double number_liters = 0.0;
    double liters_100_km = 0.0;

    printf("Enter number of miles traveled: ");
    scanf("%lf", &miles_traveled);
    printf("Enter number of gallons consumed: ");
    scanf("%lf", &number_gallons);

    miles_gallon = miles_traveled / number_gallons;
    printf("\nMiles Per Gallon: %.1lf\n", miles_gallon);

    number_liters = GALLON_FACTOR * number_gallons;
    km_travel = MILE_FACTOR * miles_traveled;
    //printf("%lf %lf\n", number_liters, km_travel);
    //  (Litres used X 100) ÷ km travelled = Litres per 100km.
    liters_100_km = (number_liters * 100.0) / km_travel;
    printf("liters-per-100-km: %.1lf\n", liters_100_km);

    return 0;
}