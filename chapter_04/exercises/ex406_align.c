/*
Write a program that requests the user’s first name and then the user’s last 
name. Have it print the entered names on one line and the number of letters in 
each name on the following line. Align each letter count with the end of the 
corresponding name, as in the following:

Melissa Honeybee
      7        8

Next, have it print the same information, but with the counts aligned with the beginning
of each name.

Melissa Honeybee
7       8
*/

#include <stdio.h>
#include <string.h>

int main(void)
{
    char first[20];
    char last[20];

    printf("Enter first name: ");
    scanf("%s", first);

    printf("Enter last name: ");
    scanf("%s", last);

    printf("%s %s\n", first, last);

    int space0 = strlen(first);
    int space1 = strlen(last);
    int width0 = space0;
    int width1 = space1;

    printf("%*d %*d\n", width0, space0, width1, space1);

    width0 = 0;
    width1 = space0;
    printf("%s %s\n", first, last);
    printf("%*d %*d\n", width0, space0, width1, space1);

    return 0;
}